""" Модуль сериализаторов справочных методов
"""
from rest_framework import serializers

from core.models import Country, Plane


class PlaneSerializer(serializers.ModelSerializer):
    # pylint: disable=too-few-public-methods
    """ Сериализатор самолета
    """

    class Meta:
        """ Meta
        """
        model = Plane
        exclude = ()


class CountrySerializer(serializers.ModelSerializer):
    # pylint: disable=too-few-public-methods
    """ Сериализатор страны
    """

    class Meta:
        """ Meta
        """
        model = Country
        fields = ('name', 'iso3166',)
