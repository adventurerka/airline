""" Модуль тестов для пользователей
"""
import copy

from django.conf import settings
from django.urls import reverse
from rest_framework import status

from api.base import factories
from api.base.test import BaseTestCase
from core.models import User, Token
from project.utils import first


class UserTestCase(BaseTestCase):
    """ Класс тестов для пользователей
    """
    user_keys = [
        'id', 'username', 'last_name', 'first_name', 'middle_name', 'user_type'
    ]

    def setUp(self):
        """ Настройка фикстур перед запуском тестов
        """
        self._user = factories.UserFactory()
        self.user_token = self.create_token(self._user)
        super().setUp()

    def _assert_token(self, response):
        """ Проверить корректность токена авторизации в ответе
        """
        # pylint: disable=no-member
        token = Token.objects.get(user_id=response.json()['id']).key
        self.assertEqual(f'Token {token}', response['Authorization'])

    def _test_user_create(
            self, request: dict, url: str, user_type: User.USER_TYPE
    ):
        # pylint: disable=no-member
        """ Тест создания пользователя
        """
        response = self.client.post(reverse(url), data=request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self._assert_token(response)
        response = response.json()
        user = User.objects.get(pk=response.pop('id'))
        self.assertTrue(
            user.check_password(request.pop('password'))
        )
        self.assertEqual(user_type, response.pop('user_type'))
        self.assertEqual(request, response)

    def _assert_account_data(self, user: User, response: dict, keys: list):
        """ Сравнить данные в БД и в ответе
        """
        for key in keys + self.user_keys:
            self.assertEqual(getattr(user, key), response.pop(key), key)
        self.assertFalse(response)

    def _test_user_login(self, user: User, keys: list):
        # pylint: disable=protected-access
        """ Тест авторизации пользователя
        """
        self.set_password(user)
        response = self.client.post(reverse('api_session'), data={
            'username': user.username, 'password': user._password
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self._assert_token(response)
        response = response.json()
        self._assert_account_data(user, response, keys)

    def _test_get_account(self, token: dict, user: User, keys: list):
        """ Тест получение информации об аккаунте пользователя
        """
        response = self.client.get(reverse('api_account'), **token)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = response.json()
        self._assert_account_data(user, response, keys)

    def _test_cannot_login(self, data: dict, keys: list):
        """ Тест ошибки авторизации пользователя
        """
        response = self.client.post(reverse('api_session'), data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self._check_error_keys(response.json(), keys)

    def _test_update(
            self, request: dict, url: str, user: User, keys: list
    ):
        # pylint: disable=(no-member, protected-access)
        """ Тест редактирования пользователя
        """

        def _test(request):
            response = self.client.put(
                reverse(url), **self.create_token(user), data=request,
                content_type='application/json'
            )
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            return response

        self.set_password(user)
        request['new_password'] = request.pop('password')
        request['old_password'] = user._password
        temp = copy.deepcopy(request)
        response = _test(request)
        user = User.objects.get(pk=user.pk)
        user._password = request.get('new_password')
        response = response.json()
        self._assert_account_data(user, copy.deepcopy(response), keys)
        response.pop('id')
        request.pop('old_password')
        self.assertTrue(user.check_password(request.pop('new_password')))
        self.assertEqual(user.user_type, response.pop('user_type'))
        self.assertEqual(request, response)

        temp['old_password'] = user._password
        _test(temp)

    def _test_cannot_update(
            self, url: str, token: dict, user: User, keys: list, request: dict
    ):
        # pylint: disable=(protected-access, too-many-arguments)
        """ Тест ошибки редактирования пользователя
        """

        def _test(request: dict, keys: list):
            response = self.client.put(
                reverse(url), **token,
                data=request,
                content_type='application/json'
            )
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
            self._check_error_keys(response.json(), keys)

        self._test_wrong_tokens(self.client.put, url)
        _test({}, ['username', 'last_name', 'first_name', *keys])
        _test(request, ['old_password'])
        request['old_password'] = request.pop('password')
        _test(request, ['old_password'])
        request['old_password'] = user._password
        _test(request, ['new_password'])
        request['new_password'] = factories.FuzzyText(
            length=settings.MIN_PASSWORD_LENGTH - 1
        ).fuzz()
        _test(request, ['new_password'])
        request['new_password'] = user._password
        request['username'] = self._user.username
        _test(request, ['username'])

    def _test_cannot_create(self, url: str, keys: list, request: dict):
        """ Тест ошибки создания пользователя
        """
        def _test(request: dict, keys: list):
            response = self.client.post(reverse(url), data=request)
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
            self._check_error_keys(response.json(), keys)

        _test({}, ['username', 'last_name', 'first_name', *keys])
        _test({**request, 'username': self._user.username}, ['username'])
        request['password'] = factories.FuzzyText(
            length=settings.MIN_PASSWORD_LENGTH - 1
        ).fuzz()
        _test(request, ['password'])
        request.pop('password')
        _test(request, ['password'])

    def _client_phone_cut(
        self, func: callable, request: dict = None, **kwargs
    ):
        # pylint: disable=no-member
        """ Проверить удаление дефисов при сохранении номера в БД
        """
        request = request or factories.ClientDictFactory()
        request['phone'] = factories.FuzzyPhone(mask='8-XXX-XXX-XX-XX').fuzz()
        response = func(reverse('api_client'), data=request, **kwargs)
        self.assertEqual(
            request['phone'].replace('-', ''),
            User.objects.get(pk=response.json()['id']).phone
        )

    def test_create_client(self):
        """ Тест создания клиента
        """
        self._test_user_create(
            factories.ClientDictFactory(), 'api_client', User.USER_TYPE.client
        )
        self._client_phone_cut(self.client.post)

    def test_create_admin(self):
        """ Тест создания админа
        """
        self._test_user_create(
            factories.AdminDictFactory(), 'api_admin', User.USER_TYPE.admin
        )

    def test_cannot_create_client(self):
        """ Тест ошибки создания клиента
        """
        self._test_cannot_create(
            'api_client', ['email', 'phone'], factories.ClientDictFactory()
        )

    def test_cannot_create_admin(self):
        """ Тест ошибки создания админа
        """
        self._test_cannot_create(
            'api_admin', ['position'], factories.AdminDictFactory()
        )

    def test_update_client(self):
        # pylint: disable=protected-access
        """ Тест редактирования клиента
        """
        self._test_update(
            factories.ClientDictFactory(), 'api_client',
            factories.ClientFactory(), ['email', 'phone']
        )
        user = factories.ClientFactory()
        request = factories.ClientDictFactory()
        self.set_password(user)
        request['new_password'] = request.pop('password')
        request['old_password'] = user._password
        self._client_phone_cut(self.client.put, request=request, **{
            **self.create_token(user), 'content_type': 'application/json'
        })

    def test_update_admin(self):
        """ Тест редактирования админа
        """
        self._test_update(
            factories.AdminDictFactory(), 'api_admin',
            factories.AdminFactory(), ['position']
        )

    def test_cannot_update_client(self):
        """ Тест ошибки редактирования клиента
        """
        self._test_cannot_update(
            'api_client', self.client_token, self._client, ['email', 'phone'],
            factories.ClientDictFactory()
        )

    def test_cannot_update_admin(self):
        """ Тест ошибки редактирования админа
        """
        self._test_cannot_update(
            'api_admin', self.admin_token, self._admin, ['position'],
            factories.AdminDictFactory()
        )

    def test_get_clients(self):
        # pylint: disable=cell-var-from-loop
        """ Тест получения списка клиентов
        """
        client = factories.ClientFactory()
        response = self.client.get(reverse('api_clients'), **self.admin_token)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = response.json()
        self.assertEqual(2, len(response))
        for obj in (self._client, client,):
            self._assert_account_data(
                obj, first(response, lambda x: x.get('id') == obj.pk),
                ['email', 'phone']
            )

    def test_cannot_get_clients(self):
        """ Тест ошибки получения списка клиентов
        """
        self._test_wrong_tokens(self.client.get, 'api_clients')
        self.assertEqual(self.client.get(
            reverse('api_clients'), **self.client_token
        ).status_code, status.HTTP_403_FORBIDDEN)

    def test_login_client(self):
        """ Тест авторизации клиента
        """
        client = factories.ClientFactory()
        self._test_user_login(client, ['email', 'phone'])
        self._test_user_login(client, ['email', 'phone'])

    def test_login_admin(self):
        """ Тест авторизации админа
        """
        admin = factories.AdminFactory()
        self._test_user_login(admin, ['position'])
        self._test_user_login(admin, ['position'])

    def test_cannot_login(self):
        """ Тест ошибки авторизации пользователя
        """
        self._test_cannot_login({}, ['username', 'password'])
        self._test_cannot_login({
            'username': factories.FuzzyCyrillic().fuzz()
        }, ['password'])
        self._test_cannot_login({
            'password': factories.FuzzyText().fuzz()
        }, ['username'])
        self._test_cannot_login({
            'username': factories.FuzzyCyrillic().fuzz(),
            'password': factories.FuzzyText().fuzz()
        }, ['username'])
        self._test_cannot_login({
            'username': self._user.username,
            'password': factories.FuzzyText().fuzz()
        }, ['password'])

    def test_client_get_account(self):
        """ Тест получение информации об аккаунте клиента
        """
        self._test_get_account(
            self.client_token, self._client, ['email', 'phone']
        )

    def test_admin_get_account(self):
        """ Тест получение информации об аккаунте админа
        """
        self._test_get_account(self.admin_token, self._admin, ['position'])

    def test_cannot_get_account(self):
        """ Тест ошибки получения информации об аккаунте без авторизации
        """
        self._test_wrong_tokens(self.client.get, 'api_account')

    def test_delete_session(self):
        # pylint: disable=no-member
        """ Тест удаления токена
        """
        token = factories.TokenFactory(user_id=factories.UserFactory().pk)
        response = self.client.delete(
            reverse('api_session'), HTTP_AUTHORIZATION=f'Token {token.key}'
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Token.objects.filter(pk=token.pk).last())

    def test_cannot_delete_session(self):
        """ Тест ошибки удаления токена без авторизации
        """
        self._test_wrong_tokens(self.client.delete, 'api_session')
