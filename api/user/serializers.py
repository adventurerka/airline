""" Модуль сериализаторов для пользователей
"""
from rest_framework import serializers

from api.base.serializers import NoModelSerializer
from core.models import User
from core.validators import PhoneValidator, PasswordValidator


class UserSerializer(serializers.ModelSerializer):
    """ Сериализатор пользователя
    """
    password = serializers.CharField(
        write_only=True, required=False, validators=[PasswordValidator()]
    )
    old_password = serializers.CharField(
        write_only=True, required=False, validators=[PasswordValidator()]
    )
    new_password = serializers.CharField(
        write_only=True, required=False, validators=[PasswordValidator()]
    )

    @staticmethod
    def _username_error(username):
        """ Ошибка валидации имени пользователя
        """
        return serializers.ValidationError({'username': [
            f'Пользователь с таким {username} пользователя уже существует.'
        ]})

    def create(self, validated_data):
        # pylint: disable=(raise-missing-from, no-member)
        """ Создание объекта пользователя
        """
        username = validated_data.get('username')
        if User.objects.filter(username=username).first():
            raise self._username_error(username)
        try:
            password = validated_data.pop('password')
            instance = super().create(validated_data)
            instance.set_password(password)
            return instance
        except KeyError:
            raise serializers.ValidationError(
                {'password': ['Обязательное поле.']}
            )

    def update(self, instance, validated_data):
        # pylint: disable=(raise-missing-from, no-member)
        """ Редактирование объекта пользователя
        """
        username = validated_data.get('username')
        user = User.objects.filter(username=username).first()
        if user and user.id != instance.id:
            raise self._username_error(username)
        try:
            old_password = validated_data.pop('old_password')
            if not instance.check_password(old_password):
                raise serializers.ValidationError({'old_password': [
                    'Указан не корректный пароль.'
                ]})
        except KeyError:
            raise serializers.ValidationError(
                {'old_password': ['Обязательное поле.']}
            )
        try:
            password = validated_data.pop('new_password')
            instance = super().update(instance, validated_data)
            instance.set_password(password)
            instance.save()
            return instance
        except KeyError:
            raise serializers.ValidationError(
                {'new_password': ['Обязательное поле.']}
            )


class AdminSerializer(UserSerializer):
    # pylint: disable=too-few-public-methods
    """ Сериализатор клиента
    """

    class Meta:
        """ Meta
        """
        model = User
        fields = (
            'id', 'first_name', 'last_name', 'middle_name', 'position',
            'username', 'password', 'user_type', 'old_password',
            'new_password',
        )

    user_type = serializers.CharField(
        required=False, default=User.USER_TYPE.admin
    )
    position = serializers.CharField()


class ClientSerializer(UserSerializer):
    # pylint: disable=too-few-public-methods
    """ Сериализатор админа
    """

    class Meta:
        """ Meta
        """
        model = User
        fields = (
            'id', 'first_name', 'last_name', 'middle_name', 'email', 'phone',
            'username', 'password', 'user_type', 'old_password',
            'new_password',
        )

    user_type = serializers.CharField(
        required=False, default=User.USER_TYPE.client
    )
    email = serializers.EmailField()
    phone = serializers.CharField(validators=[PhoneValidator()])

    def create(self, validated_data):
        """ Создание объекта пользователя
        """
        validated_data['phone'] = validated_data['phone'].replace('-', '')
        return super().create(validated_data)

    def update(self, instance, validated_data):
        """ Редактирование объекта пользователя
        """
        validated_data['phone'] = validated_data['phone'].replace('-', '')
        return super().update(instance, validated_data)


class AuthTokenSerializer(NoModelSerializer):
    """ Сериализатор запроса на авторизацию
    """
    username = serializers.CharField()
    password = serializers.CharField()

    def validate(self, attrs):
        # pylint: disable=no-member
        """ Валидация атрибутов запроса
        """
        username = attrs.get('username')
        user = User.objects.filter(username=username).first()
        if not user:
            raise serializers.ValidationError({'username': [
                f'Пользователь с таким {username} пользователя не существует.'
            ]})
        if not user.check_password(attrs.get('password')):
            raise serializers.ValidationError({'password': [
                'Указан не корректный пароль.'
            ]})
        return {**attrs, 'user': user}
