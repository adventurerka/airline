""" Файл urls модуля пользователей
"""
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^admin$', views.AdminAPIView.as_view(), name='api_admin'),
    url(r'^client$', views.ClientAPIView.as_view(), name='api_client'),
    url(r'^clients$', views.ClientListAPIView.as_view(), name='api_clients'),
    url(r'^session$', views.SessionAPIView.as_view(), name='api_session'),
    url(r'^account$', views.AccountAPIView.as_view(), name='api_account')
]
