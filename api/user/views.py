""" Модуль views для пользователей
"""
from django.contrib.auth.models import AnonymousUser
from django.db.transaction import atomic
from rest_framework import status, mixins
from rest_framework.generics import ListAPIView, CreateAPIView, GenericAPIView
from rest_framework.response import Response

from core.models import Token, User
from api.base.mixins import AdminAuthViewMixin, AuthViewMixin
from . import serializers


class UserReprMixin:
    # pylint: disable=too-few-public-methods
    """ Миксин получения json представление пользователя
    """

    @staticmethod
    def get_repr(user) -> dict:
        """ Получить json представление пользователя
        """
        return (
            serializers.ClientSerializer if user.is_client
            else serializers.AdminSerializer
        )(user).data


class TokenMixin:
    # pylint: disable=too-few-public-methods
    """ Миксин получения токена авторизации
    """

    @staticmethod
    def get_token(user) -> dict:
        # pylint: disable=no-member
        """ Получить токен авторизации
        """
        with atomic():
            token = Token.objects.filter(user=user).last()
            if token is None:
                token = Token.objects.create(user=user)
            user.save()
        return {'Authorization': f'Token {token.key}'}

    @staticmethod
    def _anonymous_error():
        return Response(
            {'detail': ['Учетные данные не были предоставлены.']},
            status=status.HTTP_401_UNAUTHORIZED
        )


class UserAPIView(TokenMixin, mixins.UpdateModelMixin, CreateAPIView):
    # pylint: disable=too-many-ancestors
    """ View создания и редактирования пользователя
    """
    _user = None

    def perform_create(self, serializer):
        """ Выполнить создание пользователя в БД
        """
        self._user = serializer.save()

    def get_success_headers(self, data) -> dict:
        """ Генерация заголовков при успешном создании пользователя
        """
        return self.get_token(self._user)

    def get_object(self) -> User:
        """ Получить исходный объект для редактирования
        """
        return self.request.user

    def put(self, request, *args, **kwargs) -> Response:
        """ Редактирование пользователя
        """
        if isinstance(self.request.user, AnonymousUser):
            return self._anonymous_error()
        return self.update(request, *args, **kwargs)


class AdminAPIView(UserAPIView):
    # pylint: disable=too-many-ancestors
    """ View создания и редактирования админа
    """
    serializer_class = serializers.AdminSerializer


class ClientAPIView(UserAPIView):
    # pylint: disable=too-many-ancestors
    """ View создания и редактирования клиента
    """
    serializer_class = serializers.ClientSerializer


class ClientListAPIView(AdminAuthViewMixin, ListAPIView):
    # pylint: disable=(too-many-ancestors, no-member)
    """ View получения списка клиентов
    """
    serializer_class = serializers.ClientSerializer
    queryset = User.objects.filter(user_type=User.USER_TYPE.client).all()


class AccountAPIView(AuthViewMixin, UserReprMixin, GenericAPIView):
    # pylint: disable=(too-many-ancestors, no-member)
    """ View получения информации о текущем пользователе
    """

    def get(self, request, *_, **__):
        # pylint: disable=unused-argument
        """ Получить информацию о текущем пользователе
        """
        return Response(self.get_repr(
            User.objects.get(pk=self.request.user.pk)
        ))


class SessionAPIView(TokenMixin, UserReprMixin, GenericAPIView):
    # pylint: disable=too-many-ancestors
    """ View создания и удаления клиента токена авторизации
    """
    serializer_class = serializers.AuthTokenSerializer

    def post(self, request, *_, **__):
        """ Получить токен авторизации
        """
        serializer = self.serializer_class(
            data=request.data, context={'request': request}
        )
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        return Response(self.get_repr(user), headers=self.get_token(user))

    def delete(self, request, *_, **__):
        # pylint: disable=no-member
        """ Удаление токена авторизации
        """
        if isinstance(request.user, AnonymousUser):
            return self._anonymous_error()
        with atomic():
            for token in Token.objects.filter(user=request.user):
                token.delete()
        return Response(
            {}, status=status.HTTP_204_NO_CONTENT,
            content_type='application/json'
        )
