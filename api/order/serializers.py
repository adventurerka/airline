# pylint: disable=duplicate-code
""" Модуль сериализаторов для заказов
"""
from django.db.transaction import atomic
from rest_framework import serializers

from core.constants import CLASS
from core.models import Flight, Order, Passenger


class PassengersSerializer(serializers.ModelSerializer):
    # pylint: disable=too-few-public-methods
    """ Сериализатор пассажира
    """

    class Meta:
        """ Meta
        """
        model = Passenger
        read_only_fields = ('id', 'price',)
        fields = read_only_fields + (
            'first_name', 'last_name', 'nationality', 'passport',
            'place_class',
        )


class OrderSerializer(serializers.ModelSerializer):
    # pylint: disable=too-few-public-methods
    """ Сериализатор заказа
    """

    class Meta:
        """ Meta
        """
        model = Order
        read_only_fields = (
            'id', 'flight_name', 'from_town', 'to_town', 'start', 'duration',
            'plane_name', 'total_price',
        )
        fields = read_only_fields + (
            'flight_settings_id', 'date', 'passengers',
        )

    flight_settings_id = serializers.IntegerField()
    date = serializers.DateField()
    passengers = PassengersSerializer(many=True)

    @atomic
    def create(self, validated_data):
        # pylint: disable=(no-member, raise-missing-from)
        """ Создание объекта заказа
        """
        passengers = validated_data.pop('passengers')
        if not passengers:
            raise serializers.ValidationError({
                'passengers': 'Список не может быть пустым.'
            })
        try:
            flight = Flight.objects.get(
                flight_settings_id=validated_data.pop('flight_settings_id'),
                date=validated_data.pop('date')
            )
        except Flight.DoesNotExist:
            raise serializers.ValidationError({
                'details': 'Не корректен flight_settings_id и/или date.'
            })

        e_places = 0
        b_places = 0
        for passenger in passengers:
            if passenger['place_class'] == CLASS.BUSINESS:
                b_places += 1
            else:
                e_places += 1
        plane = flight.flight_settings.plane

        b_places_free = plane.business_places - Passenger.objects.filter(
            order__flight_id=flight.pk, place_class=CLASS.BUSINESS
        ).count()
        e_places_free = plane.economy_places - Passenger.objects.filter(
            order__flight_id=flight.pk, place_class=CLASS.ECONOMY
        ).count()

        if b_places > b_places_free or e_places > e_places_free:
            raise serializers.ValidationError(
                {'details': 'Недостаточно свободных мест'}
            )

        instance = super().create({
            'flight': flight, 'client': self.context['request'].user
        })
        for passenger in passengers:
            instance.passengers.create(**{**passenger, 'order': instance})
        return instance
