""" Модуль views для заказов
"""
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter
from rest_framework.generics import ListCreateAPIView

from api.base.mixins import ClientOrSaveAuthViewMixin
from core.models import Order
from . import filters
from . import serializers


class ListCreateOrderAPIView(ClientOrSaveAuthViewMixin, ListCreateAPIView):
    # pylint: disable=(no-member, too-many-ancestors)
    """ View создания и получения списка заказов
    """
    serializer_class = serializers.OrderSerializer
    queryset = Order.objects.all()
    filter_backends = (SearchFilter, DjangoFilterBackend,)
    filter_class = filters.OrderFilter

    def get_queryset(self):
        if self.request.user.is_client:
            return Order.objects.filter(client=self.request.user).all()
        return Order.objects.all()
