""" Модуль тестов для заказов
"""
from django.urls import reverse
from rest_framework import status

from api.base import factories
from api.base.test import BaseWithFlightsTestCase
from core.constants import CLASS
from core.models import Order, Passenger


class UserTestCase(BaseWithFlightsTestCase):
    """ Класс тестов для заказов
    """

    def _post_order_with_bad_request(self, request: dict) -> dict:
        """ Создание заказа с ошибкой
        """
        response = self.client.post(
            reverse('api_orders'), data=request,
            **self.client_token, content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        return response.json()

    def _get_orders(self, token, params=None):
        """ Получить список заказов
        """
        params = params or {}
        response = self.client.get(
            reverse('api_orders'), **{**token, **params}
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        return response.json()

    def _get_orders_id(self, token, params=None):
        """ Получить список id заказов
        """
        return [item['id'] for item in self._get_orders(token, params)]

    def test_create_order(self):
        # pylint: disable=no-member
        """ Тест создания заказов
        """
        flight_settings = factories.FlightSettingsFactory(plane=self.plane)
        flight = factories.FlightFactory(flight_settings=flight_settings)
        flight.set_places()
        flight.save()
        request = {
            'flight_settings_id': flight_settings.id,
            'date': flight.date,
            'passengers': [
                factories.PassengerDictFactory(
                    nationality=self.country.iso3166
                ),
                factories.PassengerDictFactory(
                    nationality=self.country.iso3166, place_class=CLASS.ECONOMY
                )
            ]
        }
        response = self.client.post(
            reverse('api_orders'), data=request, **self.client_token,
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = response.json()
        order = Order.objects.get(pk=response.pop('id'))
        for key in (
                'duration', 'from_town', 'to_town', 'start', 'flight_name',
                'plane_name', 'total_price',
        ):
            self.assertEqual(response.pop(key), getattr(order, key), key)
        for passenger in response['passengers']:
            self.assertEqual(passenger.pop('price'), getattr(
                Passenger.objects.get(pk=passenger.pop('id')), 'price'
            ))
        self.assertEqual(request, response)

    def test_cannot_create_order(self):
        """ Тест ошибки создания заказов
        """
        self._test_wrong_tokens(self.client.post, 'api_orders')
        self.assertEqual(self.client.post(
            reverse('api_orders'), **self.admin_token
        ).status_code, status.HTTP_403_FORBIDDEN)

        flight = {
            'flight_settings_id': 0,
            'date': factories.FuzzyDate().fuzz()
        }

        self._check_error_keys(
            self._post_order_with_bad_request({}),
            ['flight_settings_id', 'date', 'passengers']
        )

        response = self._post_order_with_bad_request({
            **flight, 'passengers': []
        })
        self._check_error_keys(response, ['passengers'])

        response = self._post_order_with_bad_request({
            **flight, 'passengers': [{}]
        })
        self._check_error_keys(response.pop('passengers').pop(0), [
            'first_name', 'last_name', 'nationality', 'passport',
            'place_class',
        ])
        self.assertFalse(response)

        response = self._post_order_with_bad_request({
            **flight, 'passengers': [factories.PassengerDictFactory(
                nationality=factories.FuzzyText().fuzz()
            )]
        })
        response.pop('passengers').pop(0).pop('nationality')
        self.assertFalse(response)

        # Не корректен flight_settings_id и/или date.
        response = self._post_order_with_bad_request({
            **flight, 'passengers': [factories.PassengerDictFactory(
                nationality=self.country.iso3166
            )]
        })
        self._check_error_keys(response, ['details'])

        # Недостаточно свободных мест
        plane = factories.PlaneFactory(
            business_rows=1, economy_rows=1, places_in_business_row=1,
            places_in_economy_row=1
        )
        flight_settings = factories.FlightSettingsFactory(plane=plane)
        flight = factories.FlightFactory(flight_settings=flight_settings)
        response = self._post_order_with_bad_request({
            'flight_settings_id': flight_settings.pk,
            'date': flight.date,
            'passengers': [
                factories.PassengerDictFactory(
                    nationality=self.country.iso3166
                ),
                factories.PassengerDictFactory(
                    nationality=self.country.iso3166
                )
            ]
        })
        self._check_error_keys(response, ['details'])

    def test_get_orders(self):
        # pylint: disable=too-many-locals
        """ Тест получения списка заказов
        """
        order_1 = factories.OrderFactory(
            flight=self.flight, client=self._client
        )
        passenger_obj = factories.PassengerFactory(
            order=order_1, nationality=self.country
        )
        response = self._get_orders(self.admin_token)
        self.assertEqual(1, len(response))
        order = response[0]
        for key in (
                'id', 'flight_name', 'from_town', 'to_town', 'plane_name',
                'total_price', 'flight_settings_id', 'date',
        ):
            self.assertEqual(order.pop(key), getattr(order_1, key), key)
        self.assertEqual(order.pop('duration'), self.flight_settings.duration)
        self.assertEqual(order.pop('start'), self.flight_settings.start)
        passengers = order.pop('passengers')
        self.assertEqual(1, len(passengers))
        passenger = passengers[0]
        for key in (
                'id', 'price', 'first_name', 'last_name', 'passport',
                'place_class',
        ):
            self.assertEqual(
                passenger.pop(key), getattr(passenger_obj, key), key
            )
        self.assertEqual(
            passenger.pop('nationality'), passenger_obj.nationality.iso3166
        )
        self.assertFalse(passenger)
        self.assertFalse(order)

        order_2 = factories.OrderFactory(flight=self.flight, client=self._client)
        orders = [order_1.id, order_2.id]
        for token in (self.client_token, self.admin_token,):
            self.assertEqual(orders, self._get_orders_id(token))

        client = factories.ClientFactory()
        self.assertFalse(self._get_orders(self.create_token(client)))

        # todo filters

    def test_cannot_get_orders(self):
        """ Тест ошибки получения списка заказов
        """
        self._test_wrong_tokens(self.client.get, 'api_orders')
