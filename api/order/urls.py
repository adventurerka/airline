""" Файл urls модуля заказов
"""
from django.conf.urls import url

from . import views

urlpatterns = [
    url(
        r'^orders$',
        views.ListCreateOrderAPIView.as_view(),
        name='api_orders'
    )
]
