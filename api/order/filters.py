""" Модуль фильтров для заказов
"""
from django_filters import rest_framework as filters

from core.models import Order


class OrderFilter(filters.FilterSet):
    # pylint: disable=too-few-public-methods
    """ Фильтры для заказов
    """

    class Meta:
        """ Meta
        """
        model = Order
        fields = (
            'from_town', 'to_town', 'flight_name', 'plane_name', 'from_date',
            'to_date', 'client_id',
        )

    from_town = filters.CharFilter(
        field_name='data__flight__flight_settings__from_town'
    )
    to_town = filters.CharFilter(
        field_name='data__flight__flight_settings__to_town'
    )
    flight_name = filters.CharFilter(
        field_name='data__flight__flight_settings__flight_name'
    )
    plane_name = filters.CharFilter(
        field_name='data__flight__flight_settings__plane__name'
    )
    from_date = filters.CharFilter(
        field_name='data__flight__flight_settings__dates',
        lookup_expr='icontains'
    )
    to_date = filters.CharFilter(
        field_name='data__flight__flight_settings__dates',
        lookup_expr='icontains'
    )
    client_id = filters.CharFilter(field_name='client__id')
