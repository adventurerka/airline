""" Файл urls
"""
from django.conf.urls import url
from django.urls import include

from . import views

urlpatterns = [
    url(r'^', include('api.user.urls')),
    url(r'^', include('api.flight.urls')),
    url(r'^', include('api.order.urls')),
    url(r'^', include('api.place.urls')),

    url(r'^settings$', views.SettingsAPIView.as_view(), name='api_settings'),
    url(r'^planes$', views.PlaneAPIView.as_view(), name='api_planes'),
    url(r'^countries$', views.CountryAPIView.as_view(), name='api_countries')
]
