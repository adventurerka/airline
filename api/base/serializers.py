""" Базовые класс для сериализаторов
"""
from rest_framework import serializers


class NoModelSerializer(serializers.Serializer):
    """ Класс сериализатора, не привязанный к модели
    """

    def create(self, validated_data):
        """ Создать объект"""
        return self.data

    def update(self, instance, validated_data):
        """ Обновить объект
        """
        return self.data
