""" Модуль базовых тестов
"""
from django.test import TestCase
from django.urls import reverse
from rest_framework import status

from api.base import factories
from core.models import User


class BaseTestCase(TestCase):
    """ Базовый класс тестов для views
    """

    @staticmethod
    def set_password(obj):
        # pylint: disable=protected-access
        """ Хешировать пароль и сохранить копию оригинального
        """
        password = obj.password
        obj.set_password(password)
        obj.save()
        obj._password = password

    def create_token(self, user: User) -> dict:
        """ Сгенерировать в БД и вернуть токен для пользователя
        """
        return self._get_token_header(
            factories.TokenFactory(user_id=user.pk).key
        )

    def setUp(self):
        """ Настройка фикстур перед запуском тестов
        """
        self._admin = factories.AdminFactory()
        self.admin_token = self.create_token(self._admin)
        self.set_password(self._admin)

        self._client = factories.ClientFactory()
        self.client_token = self.create_token(self._client)
        self.set_password(self._client)

    @staticmethod
    def _get_token_header(token: str) -> dict:
        """ Сгенерировать на основе токена хидер авторизации
        """
        return {'HTTP_AUTHORIZATION': f'Token {token}'}

    def _test_wrong_tokens(self, func: callable, url: str, kwargs=None):
        """ Тест ошибки не корректного токена
        """
        for token in (
            {}, self._get_token_header(factories.FuzzyUUID().fuzz()),
        ):
            self.assertEqual(
                func(reverse(url, kwargs=kwargs), **token).status_code,
                status.HTTP_401_UNAUTHORIZED
            )

    def _check_error_keys(self, response: dict, keys: list):
        """ Проверить наличии в ответе только ожидаемых полей с ошибками
        """
        for key in keys:
            response.pop(key)
        self.assertFalse(response)


class BaseWithFlightsTestCase(BaseTestCase):
    """ Базовый класс тестов для views с рейсами
    """

    def setUp(self):
        """ Настройка фикстур перед запуском тестов
        """
        super().setUp()
        self.country = factories.CountryFactory()
        self.plane = factories.PlaneFactory()
        self.flight_settings = factories.FlightSettingsFactory(
            plane=self.plane
        )
        self.flight = factories.FlightFactory(
            flight_settings=self.flight_settings
        )
