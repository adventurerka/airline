""" Модуль генерации тестовых данных
"""
import datetime
import random
import string
import uuid

import factory
from django.conf import settings
from factory.fuzzy import FuzzyText, BaseFuzzyAttribute

from core.constants import CLASS
from core.models import (
    User, Token, Plane, FlightSettings, Country, Passenger, Flight, Order,
)


class FuzzyEmail(BaseFuzzyAttribute):
    """ Генератор почты
    """

    def __init__(self, user_name: int = 6, host: int = 6, domain: int = 2):
        super().__init__()
        self.user_name = user_name
        self.host = host
        self.domain = domain

    def fuzz(self):
        """ Метод генерации
        """
        values = [FuzzyText(
            length=x, chars=string.digits + string.ascii_letters
        ).fuzz() for x in (self.user_name, self.host,)]
        values.append(FuzzyText(length=self.domain).fuzz())
        return '{}@{}.{}'.format(*values)


class FuzzyMaskedText(BaseFuzzyAttribute):
    """ Генератор строки по маске
    """

    def __init__(self, chars: str, mask: str, mask_char: str = 'X'):
        super().__init__()
        self.chars = chars
        self.mask = mask
        self.mask_char = mask_char

    def fuzz(self):
        """ Метод генерации
        """
        return ''.join(
            random.choice(self.chars) if ch == self.mask_char
            else ch for ch in list(self.mask)
        )


class FuzzyPhone(FuzzyMaskedText):
    """ Генератор номера телефона
    """

    def __init__(self, mask: str = '+7XXXXXXXXXX'):
        super().__init__(string.digits, mask)


class FuzzyCyrillic(FuzzyText):
    """ Генератор строки из кириллицы
    """

    def __init__(self):
        cyrillic_lowercase = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'
        cyrillic = f'{cyrillic_lowercase}{cyrillic_lowercase.upper()}- '
        super().__init__(chars=cyrillic)

    def fuzz(self):
        """ Метод генерации
        """
        return super().fuzz().strip()


class FuzzyUUID(BaseFuzzyAttribute):
    """ Генератор uuid
    """

    def __init__(self, uuid_type: str = 'uuid4'):
        super().__init__()
        self.uuid_type = uuid_type

    def fuzz(self) -> str:
        """ Метод генерации
        """
        return str(getattr(uuid, self.uuid_type)())


class FuzzyInt(BaseFuzzyAttribute):
    """ Генератор случайной цифры от 1 до 9
    """

    def fuzz(self) -> int:
        """ Метод генерации
        """
        return random.randint(1, 9)


class FuzzyDate(BaseFuzzyAttribute):
    """ Генератор даты формата %Y-%m-%d с delta: int относительно текущей
    """

    def __init__(self, delta: int = 0):
        super().__init__()
        self.delta = delta

    def fuzz(self) -> str:
        """ Метод генерации
        """
        return (
                datetime.datetime.now() + datetime.timedelta(days=self.delta)
        ).strftime(settings.DATE_FORMAT)


class UserFactory(factory.django.DjangoModelFactory):
    # pylint: disable=too-few-public-methods
    """ Фабрика объекта пользователь
    """

    class Meta:
        """ Meta
        """
        model = User

    username = FuzzyCyrillic()
    first_name = FuzzyText()
    last_name = FuzzyText()
    middle_name = FuzzyText()
    password = FuzzyText()


class ClientFactory(UserFactory):
    """ Фабрика объекта пользователь - клиент
    """
    email = FuzzyEmail()
    phone = FuzzyPhone()
    user_type = User.USER_TYPE.client


class AdminFactory(UserFactory):
    """ Фабрика объекта пользователь - админ
    """
    position = FuzzyText()
    user_type = User.USER_TYPE.admin


class TokenFactory(factory.django.DjangoModelFactory):
    # pylint: disable=too-few-public-methods
    """ Фабрика объекта токен
    """

    class Meta:
        """ Meta
        """
        model = Token

    key = FuzzyUUID()


class UserDictFactory(factory.DictFactory):
    """ Фабрика json запроса пользователь
    """
    username = FuzzyCyrillic()
    first_name = FuzzyText()
    last_name = FuzzyText()
    middle_name = FuzzyText()
    password = FuzzyText()


class ClientDictFactory(UserDictFactory):
    """ Фабрика json запроса клиент
    """
    email = FuzzyEmail()
    phone = FuzzyPhone()


class AdminDictFactory(UserDictFactory):
    """ Фабрика json запроса админ
    """
    position = FuzzyText()


class PlaneFactory(factory.django.DjangoModelFactory):
    # pylint: disable=too-few-public-methods
    """ Фабрика объекта самолёт
    """

    class Meta:
        """ Meta
        """
        model = Plane

    name = FuzzyText()
    business_rows = FuzzyInt()
    economy_rows = FuzzyInt()
    places_in_business_row = FuzzyInt()
    places_in_economy_row = FuzzyInt()


class ScheduleDict(factory.DictFactory):
    """ Фабрика json запроса расписания рейса
    """
    from_date = FuzzyDate()
    to_date = FuzzyDate()
    period = 'DAILY'


class FlightSettingsFactory(factory.django.DjangoModelFactory):
    # pylint: disable=too-few-public-methods
    """ Фабрика объекта настройки рейса
    """

    class Meta:
        """ Meta
        """
        model = FlightSettings

    flight_name = FuzzyText()
    from_town = FuzzyText()
    to_town = FuzzyText()
    start = datetime.datetime.now().strftime('%H:%M')
    duration = datetime.datetime.now().strftime('%H:%M')
    price_business = FuzzyInt()
    price_economy = FuzzyInt()
    schedule = dict()
    dates = [FuzzyDate().fuzz(), FuzzyDate(1).fuzz()]


class FlightFactory(factory.django.DjangoModelFactory):
    # pylint: disable=too-few-public-methods
    """ Фабрика объекта рейс
    """

    class Meta:
        """ Meta
        """
        model = Flight

    date = FuzzyDate().fuzz()


class FlightSettingsDictFactory(factory.DictFactory):
    """ Фабрика json запроса настройки рейса
    """
    flight_name = FuzzyText()
    from_town = FuzzyText()
    to_town = FuzzyText()
    start = datetime.datetime.now().strftime(settings.TIME_FORMAT)
    duration = datetime.datetime.now().strftime(settings.TIME_FORMAT)
    price_business = FuzzyInt()
    price_economy = FuzzyInt()
    dates = [FuzzyDate().fuzz(), FuzzyDate(1).fuzz()]


class CountryFactory(factory.django.DjangoModelFactory):
    # pylint: disable=too-few-public-methods
    """ Фабрика объекта страна
    """
    class Meta:
        """ Meta
        """
        model = Country

    iso3166 = FuzzyText()
    name = FuzzyText()


class PassengerFactory(factory.django.DjangoModelFactory):
    # pylint: disable=too-few-public-methods
    """ Фабрика объекта пассажир
    """
    class Meta:
        """ Meta
        """
        model = Passenger

    first_name = FuzzyText()
    last_name = FuzzyText()
    nationality = CountryFactory()
    passport = FuzzyText()
    place_class = CLASS.BUSINESS


class PassengerDictFactory(factory.DictFactory):
    """ Фабрика json запроса пассажир
    """
    first_name = FuzzyText()
    last_name = FuzzyText()
    passport = FuzzyText()
    place_class = CLASS.BUSINESS


class PassengerRegisterDictFactory(factory.DictFactory):
    """ Фабрика json запроса регистрации пассажира
    """
    first_name = FuzzyText()
    last_name = FuzzyText()
    place = FuzzyText()
    order_id = FuzzyInt()


class OrderFactory(factory.django.DjangoModelFactory):
    # pylint: disable=too-few-public-methods
    """ Фабрика объекта заказ
    """

    class Meta:
        """ Meta
        """
        model = Order
