""" Миксины для api
"""
from rest_framework.permissions import IsAuthenticated

from api.base.constants import SAVE_METHODS


class IsClient(IsAuthenticated):
    """ Праверка авторизации клиента
    """

    def has_permission(self, request, view):
        """ Праверка наличия у пользователя прав на вызываемыый метод
        """
        return super().has_permission(request, view) and request.user.is_client


class IsAdmin(IsAuthenticated):
    """ Праверка авторизации админа
    """

    def has_permission(self, request, view):
        """ Праверка наличия у пользователя прав на вызываемыый метод
        """
        return super().has_permission(request, view) and request.user.is_admin


class IsAdminOrSave(IsAuthenticated):
    """ Миксин проверки наличия авторизации админа
    или иного типа пользователя, если метод в SAVE_METHODS
    """

    def has_permission(self, request, view):
        """ Праверка наличия у пользователя прав на вызываемыый метод
        """
        return super().has_permission(request, view) and (
                request.user.is_admin or request.method in SAVE_METHODS
        )


class IsClientOrSave(IsAuthenticated):
    """ Миксин проверки наличия авторизации клиента
    или иного типа пользователя, если метод в SAVE_METHODS
    """

    def has_permission(self, request, view):
        """ Праверка наличия у пользователя прав на вызываемыый метод
        """
        return super().has_permission(request, view) and (
                request.user.is_client or request.method in SAVE_METHODS
        )


class AuthViewMixin:
    # pylint: disable=too-few-public-methods
    """ Миксин проверки наличия авторизации в запросе
    """
    permission_classes = (IsAuthenticated,)


class AdminAuthViewMixin(AuthViewMixin):
    # pylint: disable=too-few-public-methods
    """ Миксин проверки авторизации админа
    """
    permission_classes = (IsAdmin,)


class AdminOrSaveAuthViewMixin(AuthViewMixin):
    # pylint: disable=too-few-public-methods
    """ Миксин проверки наличия авторизации админа
    или иного типа пользователя, если метод в SAVE_METHODS
    """
    permission_classes = (IsAdminOrSave,)


class ClientAuthViewMixin(AuthViewMixin):
    # pylint: disable=too-few-public-methods
    """ Миксин проверки авторизации клиента
    """
    permission_classes = (IsClient,)


class ClientOrSaveAuthViewMixin(AuthViewMixin):
    # pylint: disable=too-few-public-methods
    """ Миксин проверки наличия авторизации клиента
    или иного типа пользователя, если метод в SAVE_METHODS
    """
    permission_classes = (IsClientOrSave,)
