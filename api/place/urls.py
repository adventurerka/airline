""" Файл urls модуля регистрации пассажиров
"""
from django.conf.urls import url

from . import views

urlpatterns = [
    url(
        r'^places$',
        views.RegisterPassengerAPIView.as_view(),
        name='api_register_passenger'
    ),
    url(
        r'^places/(?P<pk>[0-9]+)$',
        views.RetrievePlaceAPIView.as_view(),
        name='api_retrieve_place'
    )
]
