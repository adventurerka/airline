""" Модуль сериализаторов для регистрации пассажиров
"""
from django.db.transaction import atomic
from rest_framework import serializers

from core.models import Passenger


class PlaceSerializer(serializers.ModelSerializer):
    # pylint: disable=too-few-public-methods
    """ Сериализатор регистрации пассажира
    """

    class Meta:
        """ Meta
        """
        model = Passenger
        fields = ('id', 'order_id', 'first_name', 'last_name', 'place',)

    id = serializers.IntegerField()
    order_id = serializers.IntegerField()
    place = serializers.CharField()

    @atomic
    def update(self, instance, validated_data):
        # pylint: disable=raise-missing-from
        """ Присвоение пассажиру места в самолете
        """
        flight = instance.order.flight
        place = validated_data['place']
        try:
            flight.free_place_codes[instance.place_class].remove(place)
            flight.save()
        except ValueError:
            raise serializers.ValidationError({'place': ['Место не найдено']})
        instance.place = place
        instance.save()
        return instance
