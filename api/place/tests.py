""" Модуль тестов для мест
"""
from django.urls import reverse
from rest_framework import status

from api.base import factories
from api.base.test import BaseWithFlightsTestCase
from core.constants import CLASS
from core.models import Flight


class UserTestCase(BaseWithFlightsTestCase):
    """ Класс тестов для мест
    """

    def setUp(self):
        """ Настройка фикстур перед запуском тестов
        """
        super().setUp()
        self.flight.set_places()
        self.flight.save()
        self.order = factories.OrderFactory(
            flight=self.flight, client=self._client
        )

    def test_get_places(self):
        """ Тест получения списка доступных мест
        """
        response = self.client.get(
            reverse('api_retrieve_place', kwargs={'pk': self.order.pk}),
            **self.client_token
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(),
            self.flight.free_place_codes[CLASS.BUSINESS]
            + self.flight.free_place_codes[CLASS.ECONOMY]
        )

    def test_cannot_get_places(self):
        """ Тест ошибки получения списка доступных мест
        """
        self._test_wrong_tokens(
            self.client.get, 'api_retrieve_place', {'pk': 0}
        )

        self.assertEqual(self.client.get(
            reverse('api_retrieve_place', kwargs={'pk': 0}),
            **self.admin_token
        ).status_code, status.HTTP_403_FORBIDDEN)

        self.assertEqual(self.client.get(
            reverse('api_retrieve_place', kwargs={'pk': 0}),
            **self.client_token
        ).status_code, status.HTTP_404_NOT_FOUND)

        order = factories.OrderFactory(
            flight=self.flight, client=factories.ClientFactory()
        )
        self.assertEqual(self.client.get(
            reverse('api_retrieve_place', kwargs={'pk': order.pk}),
            **self.client_token
        ).status_code, status.HTTP_404_NOT_FOUND)

    def test_register_passenger(self):
        # pylint: disable=no-member
        """ Тест регистрации пассажира на рейс
        """
        passenger = factories.PassengerFactory(
            order=self.order, nationality=self.country
        )
        request = factories.PassengerRegisterDictFactory()
        places_src = self.flight.free_place_codes[CLASS.BUSINESS]
        request['place'] = places_src[0]
        request['id'] = passenger.id
        response = self.client.post(
            reverse('api_register_passenger'), data=request,
            **self.client_token
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = response.json()
        for key in ('last_name', 'first_name', 'order_id',):
            self.assertIsNotNone(response.pop(key), key)
            request.pop(key)
        self.assertEqual(request, response)
        places_dst = Flight.objects.get(
            pk=self.flight.pk
        ).free_place_codes[CLASS.BUSINESS]
        self.assertFalse(request['place'] in places_dst)
        self.assertEqual(len(places_dst) + 1, len(places_src))

    def test_cannot_register_passenger(self):
        """ Тест ошибки регистрации пассажира на рейс
        """
        self._test_wrong_tokens(self.client.post, 'api_register_passenger')
        self.assertEqual(self.client.post(
            reverse('api_register_passenger'), **self.admin_token
        ).status_code, status.HTTP_403_FORBIDDEN)
        response = self.client.post(
            reverse('api_register_passenger'), data={},
            **self.client_token
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self._check_error_keys(
            response.json(),
            ['id', 'order_id', 'first_name', 'last_name', 'place']
        )

        request = factories.PassengerRegisterDictFactory()
        request['id'] = 0
        self.assertEqual(self.client.post(
            reverse('api_register_passenger'), data=request,
            **self.client_token
        ).status_code, status.HTTP_404_NOT_FOUND)

        request['id'] = factories.PassengerFactory(
            order=self.order, nationality=self.country,
            place=factories.FuzzyText().fuzz()
        ).id
        self.assertEqual(self.client.post(
            reverse('api_register_passenger'), data=request,
            **self.client_token
        ).status_code, status.HTTP_404_NOT_FOUND)

        order = factories.OrderFactory(
            flight=self.flight, client=factories.ClientFactory()
        )
        request['id'] = factories.PassengerFactory(
            order=order, nationality=self.country
        ).id
        self.assertEqual(self.client.post(
            reverse('api_register_passenger'), data=request,
            **self.client_token
        ).status_code, status.HTTP_404_NOT_FOUND)

        request['id'] = factories.PassengerFactory(
            order=self.order, nationality=self.country
        ).id
        response = self.client.post(
            reverse('api_register_passenger'), data=request,
            **self.client_token
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self._check_error_keys(response.json(), ['place'])
