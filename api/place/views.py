""" Модуль views для регистрации пассажиров
"""
from rest_framework import status
from rest_framework.generics import GenericAPIView, RetrieveAPIView
from rest_framework.response import Response

from api.base.mixins import ClientAuthViewMixin
from core.constants import CLASS
from core.models import Order, Passenger
from . import serializers


class RegisterPassengerAPIView(ClientAuthViewMixin, GenericAPIView):
    """ View регистрации пассажира
    """
    serializer_class = serializers.PlaceSerializer

    def post(self, request, *_, **__):
        # pylint: disable=no-member
        """ Регистрации пассажира
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = Passenger.objects.filter(
            id=request.data['id'], place=None, order__client_id=request.user.id
        ).first()
        if instance is None:
            return Response(
                {'detail': 'Страница не найдена.'},
                status=status.HTTP_404_NOT_FOUND
            )
        serializer.instance = instance
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)


class RetrievePlaceAPIView(ClientAuthViewMixin, RetrieveAPIView):
    # pylint: disable=(no-member, too-many-ancestors)
    """ View получения списка свободных мест на рейсе для заказа
    """
    queryset = Order.objects.all()

    def retrieve(self, request, *args, **kwargs):
        """ Получение списка свободных мест на рейсе для заказа
        """
        instance = self.get_object()
        if instance.client != request.user:
            return Response(
                {'detail': 'Страница не найдена.'},
                status=status.HTTP_404_NOT_FOUND
            )
        flight = instance.flight
        return Response(
            flight.free_place_codes[CLASS.BUSINESS] +
            flight.free_place_codes[CLASS.ECONOMY]
        )
