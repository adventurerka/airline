""" Модуль тестов
"""
from django.conf import settings
from django.urls import reverse
from rest_framework import status

from api.base import factories
from api.base.test import BaseTestCase


class HandbookTestCase(BaseTestCase):
    """ Класс тестов справочных методов
    """

    def test_get_settings(self):
        """ Тест получения настроек сервера
        """
        response = self.client.get(reverse('api_settings'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual({
            'max_name_length': settings.MAX_NAME_LENGTH,
            'min_password_length': settings.MIN_PASSWORD_LENGTH
        }, response.json())

    def test_get_planes(self):
        """ Тест получения списка самолётов
        """
        planes = [factories.PlaneFactory() for _ in range(2)]
        response = self.client.get(reverse('api_planes'), **self.admin_token)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual([{key: getattr(plane, key) for key in (
            'name', 'business_rows', 'economy_rows', 'places_in_business_row',
            'places_in_economy_row',
        )} for plane in planes], response.json())

    def test_cannot_get_planes(self):
        """ Тест ошибки получения списка самолётов
        """
        self._test_wrong_tokens(self.client.get, 'api_planes')
        self.assertEqual(self.client.get(
            reverse('api_planes'), **self.client_token
        ).status_code, status.HTTP_403_FORBIDDEN)

    def test_get_countries(self):
        """ Тест получения списка стран
        """
        countries = [factories.CountryFactory() for _ in range(2)]
        response = self.client.get(reverse('api_countries'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual([{key: getattr(country, key) for key in (
            'name', 'iso3166',
        )} for country in countries], response.json())
