""" Файл urls модуля настроек рейсов
"""
from django.conf.urls import url

from . import views

urlpatterns = [
    url(
        r'^flights$',
        views.ListCreateFlightSettingsAPIView.as_view(),
        name='api_list_create_flight'
    ),
    url(
        r'^flights/(?P<pk>[0-9]+)$',
        views.FlightAPIView.as_view(),
        name='api_flight'
    ),
    url(
        r'^flights/(?P<pk>[0-9]+)/approve$',
        views.FlightApproveAPIView.as_view(),
        name='api_approve_flight'
    )
]
