""" Модуль фильтров для настроек рейсов
"""
from django_filters import rest_framework as filters

from core.models import FlightSettings


class FlightFilter(filters.FilterSet):
    # pylint: disable=too-few-public-methods
    """ Фильтры для настроек рейсов
    """

    class Meta:
        """ Meta
        """
        model = FlightSettings
        fields = (
            'plane_name', 'flight_name', 'from_town', 'to_town', 'from_date',
            'to_date',
        )

    plane_name = filters.CharFilter(field_name='plane__name')
    from_date = filters.DateFilter(field_name='dates', lookup_expr='icontains')
    to_date = filters.DateFilter(field_name='dates', lookup_expr='icontains')
