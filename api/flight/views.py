""" Модуль views для настроек рейсов
"""
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter
from rest_framework.generics import (
    ListCreateAPIView, DestroyAPIView, RetrieveAPIView, GenericAPIView,
)
from rest_framework.mixins import UpdateModelMixin
from rest_framework.response import Response

from api.base.constants import SAVE_METHODS
from api.base.mixins import AdminAuthViewMixin, AdminOrSaveAuthViewMixin
from core.models import FlightSettings
from . import filters
from . import serializers


class ListCreateFlightSettingsAPIView(
    AdminOrSaveAuthViewMixin, ListCreateAPIView
):
    # pylint: disable=too-many-ancestors
    """ View создания и получения списка настроек рейса
    """
    serializer_class = serializers.FlightSerializer
    filter_backends = (SearchFilter, DjangoFilterBackend,)
    filter_class = filters.FlightFilter

    def get_queryset(self):
        # pylint: disable=no-member
        """ Получить queryset для view
        """
        if self.request.user.is_client:
            return FlightSettings.objects.filter(is_approved=True).all()
        return FlightSettings.objects.all()


class FlightAPIView(
    AdminAuthViewMixin, DestroyAPIView, RetrieveAPIView, UpdateModelMixin
):
    # pylint: disable=too-many-ancestors
    """ View удаления, редактирования и получения настроек рейса
    """
    serializer_class = serializers.FlightSerializer

    def get_queryset(self):
        # pylint: disable=no-member
        """ Получить queryset для view
        """
        if self.request.method not in SAVE_METHODS:
            return FlightSettings.objects.filter(is_approved=False).all()
        return FlightSettings.objects.all()

    def put(self, request, *args, **kwargs):
        """ Редактирование настроек рейсов
        """
        return self.update(request, *args, **kwargs)


class FlightApproveAPIView(AdminAuthViewMixin, GenericAPIView):
    # pylint: disable=no-member
    """ View утверждения настроек рейса
    """
    serializer_class = serializers.FlightApproveSerializer
    queryset = FlightSettings.objects.filter(is_approved=False).all()

    def put(self, request, *_, **__):
        """ Утверждения настроек рейса
        """
        serializer = self.get_serializer(self.get_object(), data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)
