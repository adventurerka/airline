""" Модуль сериализаторов для настроек рейсов
"""
from django.conf import settings
from django.db.transaction import atomic
from rest_framework import serializers

from api.base.serializers import NoModelSerializer
from core.models import FlightSettings, Flight
from core.validators import PeriodValidator


class ScheduleSerializer(NoModelSerializer):
    # pylint: disable=too-few-public-methods
    """ Сериализатор расписания рейса
    """

    class Meta:
        """ Meta
        """
        fields = ('from_date', 'to_date', 'period',)

    from_date = serializers.DateField()
    to_date = serializers.DateField()
    period = serializers.CharField(validators=[PeriodValidator()])


class FlightSerializer(serializers.ModelSerializer):
    # pylint: disable=too-few-public-methods
    """ Сериализатор настроек рейса
    """

    class Meta:
        """ Meta
        """
        model = FlightSettings
        read_only_fields = ('id', 'is_approved',)
        fields = read_only_fields + (
            'flight_name', 'plane', 'from_town', 'to_town', 'start',
            'duration', 'price_business', 'price_economy', 'schedule', 'dates',
        )

    start = serializers.TimeField(format=settings.TIME_FORMAT)
    duration = serializers.TimeField(format=settings.TIME_FORMAT)
    schedule = serializers.JSONField(default=dict())
    dates = serializers.ListField(
        child=serializers.DateField(), required=False
    )

    def validate(self, attrs):
        """ Валидирование расписания рейса
        """
        schedule = attrs.get('schedule')
        dates = attrs.get('dates')
        if (not schedule and not dates) or (schedule and dates):
            raise serializers.ValidationError({
                'details': [
                    'Обязательно указывается только одно значение'
                    ' schedule или dates.'
                ]
            })
        if schedule:
            schedule_serializer = ScheduleSerializer(data=schedule)
            schedule_serializer.is_valid(raise_exception=True)
            attrs['dates'] = FlightSettings.get_days_from_schedule(schedule)
            return attrs
        attrs['dates'] = [day.isoformat() for day in dates]
        return attrs


class FlightApproveSerializer(serializers.ModelSerializer):
    # pylint: disable=too-few-public-methods
    """ Сериализатор утверждения настроек рейса
    """

    class Meta:
        """ Meta
        """
        model = FlightSettings
        read_only_fields = (
            'id', 'flight_name', 'plane', 'from_town', 'to_town', 'start',
            'duration', 'price_business', 'price_economy', 'is_approved',
            'schedule', 'dates',
        )
        fields = read_only_fields

    start = serializers.TimeField(format=settings.TIME_FORMAT, read_only=True)
    duration = serializers.TimeField(
        format=settings.TIME_FORMAT, read_only=True
    )

    @atomic
    def update(self, instance, validated_data):
        """ Утвертить настройки рейса
        """
        validated_data['is_approved'] = True
        instance = super().update(instance, validated_data)
        for date in instance.dates:
            flight = Flight(date=date, flight_settings=instance)
            flight.set_places()
            flight.save()
        return instance
