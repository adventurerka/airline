""" Модуль тестов для рейсов
"""
import datetime

from django.conf import settings
from django.urls import reverse
from rest_framework import status

from api.base import factories
from api.base.test import BaseTestCase
from core.constants import CLASS
from core.models import FlightSettings, Flight


class UserTestCase(BaseTestCase):
    """ Класс тестов для рейсов
    """
    flight_settings_keys = [
        'id', 'flight_name', 'plane', 'from_town', 'to_town', 'start',
        'duration', 'price_business', 'price_economy', 'dates', 'is_approved'
    ]

    def setUp(self):
        """ Настройка фикстур перед запуском тестов
        """
        super().setUp()
        self.plane = factories.PlaneFactory()

    def get_flight_settings(self) -> FlightSettings:
        """ Создать объект настроек рейсов в БД
        """
        return factories.FlightSettingsFactory(plane_id=self.plane.pk)

    def _assert_flight_data(
            self, flight: FlightSettings, response: dict, keys: list
    ):
        """ Сравнить данные в БД и в ответе
        """
        for key in keys + self.flight_settings_keys:
            if key == 'plane':
                value = flight.plane.name
            else:
                value = getattr(flight, key)
                if isinstance(value, datetime.time):
                    value = value.strftime(settings.TIME_FORMAT)
            self.assertEqual(value, response.pop(key), key)
        self.assertFalse(response.pop('schedule', None))
        self.assertFalse(response)

    def _list_flights_settings(self, token, params=None):
        """ Получить список настроек рейсов
        """
        params = params or {}
        response = self.client.get(
            reverse('api_list_create_flight'), **{**token, **params}
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        return response.json()

    def _list_flights_settings_id(self, token, params=None):
        """ Получить список id настроек рейсов
        """
        return [
            item['id'] for item in self._list_flights_settings(token, params)
        ]

    def test_create_flight_settings(self):
        # pylint: disable=no-member
        """ Тест создания настроек рейса
        """
        request = factories.FlightSettingsDictFactory(plane=self.plane.name)
        response = self.client.post(
            reverse('api_list_create_flight'), **self.admin_token,
            data=request
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = response.json()
        FlightSettings.objects.get(pk=response.pop('id'))
        self.assertFalse(response.pop('is_approved'))
        self.assertFalse(response.pop('schedule'))
        self.assertEqual(request, response)

    def test_cannot_create_flight_settings(self):
        """ Тест ошибки создания настроек рейса
        """
        self._test_wrong_tokens(self.client.post, 'api_list_create_flight')
        self.assertEqual(self.client.post(
            reverse('api_list_create_flight'), **self.client_token
        ).status_code, status.HTTP_403_FORBIDDEN)

        def _test(request: dict) -> dict:
            response = self.client.post(
                reverse('api_list_create_flight'), data=request,
                **self.admin_token, content_type='application/json'
            )
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
            return response.json()

        self._check_error_keys(_test({}), [
            'flight_name', 'from_town', 'to_town', 'start', 'duration',
            'price_business', 'price_economy', 'plane'
        ])
        request = factories.FlightSettingsDictFactory(plane=self.plane.name)
        request['schedule'] = factories.ScheduleDict()
        self._check_error_keys(_test(request), ['details'])
        request.pop('dates')
        request.pop('schedule')
        self._check_error_keys(_test(request), ['details'])
        request['schedule'] = {'': ''}
        self._check_error_keys(
            _test(request), ['from_date', 'to_date', 'period']
        )

    def test_cannot_update_flight_settings(self):
        """ Тест ошибки редактирования настроек рейса
        """
        self._test_wrong_tokens(self.client.put, 'api_flight', {'pk': 0})
        self.assertEqual(self.client.put(
            reverse('api_flight', kwargs={'pk': 0}), **self.client_token
        ).status_code, status.HTTP_403_FORBIDDEN)

        self.assertEqual(self.client.put(
            reverse('api_flight', kwargs={'pk': 0}), **self.admin_token
        ).status_code, status.HTTP_404_NOT_FOUND)

        f_settings = self.get_flight_settings()
        f_settings.is_approved = True
        f_settings.save()
        self.assertEqual(self.client.put(
            reverse('api_flight', kwargs={'pk': f_settings.pk}),
            **self.admin_token
        ).status_code, status.HTTP_404_NOT_FOUND)

        f_settings = self.get_flight_settings()
        f_settings.is_approved = False
        f_settings.save()

        def _test(request: dict) -> dict:
            response = self.client.put(
                reverse('api_flight', kwargs={'pk': f_settings.pk}),
                data=request, **self.admin_token,
                content_type='application/json'
            )
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
            return response.json()

        self._check_error_keys(_test({}), [
            'flight_name', 'from_town', 'to_town', 'start', 'duration',
            'price_business', 'price_economy', 'plane'
        ])
        request = factories.FlightSettingsDictFactory(plane=self.plane.name)
        request['schedule'] = factories.ScheduleDict()
        self._check_error_keys(_test(request), ['details'])
        request.pop('dates')
        request.pop('schedule')
        self._check_error_keys(_test(request), ['details'])
        request['schedule'] = {'': ''}
        self._check_error_keys(
            _test(request), ['from_date', 'to_date', 'period']
        )

    def test_delete_flight_settings(self):
        # pylint: disable=no-member
        """ Тест удаления настроек рейса
        """
        f_settings = self.get_flight_settings()
        response = self.client.delete(
            reverse('api_flight', kwargs={'pk': f_settings.pk}),
            **self.admin_token
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(
            FlightSettings.objects.filter(pk=f_settings.pk).last())

    def test_cannot_delete_flight_settings(self):
        """ Тест ошибки удаления настроек рейса
        """
        self._test_wrong_tokens(self.client.delete, 'api_flight', {'pk': 0})
        self.assertEqual(self.client.delete(
            reverse('api_flight', kwargs={'pk': 0}), **self.client_token
        ).status_code, status.HTTP_403_FORBIDDEN)

        self.assertEqual(self.client.delete(
            reverse('api_flight', kwargs={'pk': 0}), **self.admin_token
        ).status_code, status.HTTP_404_NOT_FOUND)

        f_settings = self.get_flight_settings()
        f_settings.is_approved = True
        f_settings.save()
        self.assertEqual(self.client.delete(
            reverse('api_flight', kwargs={'pk': f_settings.pk}),
            **self.admin_token
        ).status_code, status.HTTP_404_NOT_FOUND)

    def test_approve_flight_settings(self):
        # pylint: disable=no-member
        """ Тест утверждения настроек рейса
        """

        def test(settings_pk: int, keys: list):
            response = self.client.put(
                reverse('api_approve_flight', kwargs={'pk': settings_pk}),
                **self.admin_token
            )
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            f_settings = FlightSettings.objects.get(pk=settings_pk)
            self._assert_flight_data(f_settings, response.json(), keys)
            return f_settings.pk

        request = self.get_flight_settings()
        flights = Flight.objects.filter(
            flight_settings_id=test(request.pk, [])
        ).all()
        self.assertEqual([
            flight.date.strftime(settings.DATE_FORMAT) for flight in flights
        ], request.dates)
        plane = request.plane
        economy_places = plane.economy_rows * plane.places_in_economy_row
        business_places = plane.business_rows * plane.places_in_business_row
        for flight in flights:
            self.assertEqual(
                len(flight.free_place_codes[CLASS.BUSINESS]), business_places
            )
            self.assertEqual(
                len(flight.free_place_codes[CLASS.ECONOMY]), economy_places
            )

        request = self.get_flight_settings()
        request.schedule = factories.ScheduleDict()
        request.save()
        test(request.pk, ['schedule'])

    def test_cannot_approve_flight_settings(self):
        """ Тест ошибки утверждения настроек рейса
        """
        self._test_wrong_tokens(
            self.client.put, 'api_approve_flight', {'pk': 0}
        )
        self.assertEqual(self.client.put(
            reverse('api_approve_flight', kwargs={'pk': 0}),
            **self.client_token
        ).status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(self.client.put(
            reverse('api_approve_flight', kwargs={'pk': 0}),
            **self.admin_token
        ).status_code, status.HTTP_404_NOT_FOUND)
        flight = self.get_flight_settings()
        flight.is_approved = True
        flight.save()
        self.assertEqual(self.client.put(
            reverse('api_approve_flight', kwargs={'pk': flight.pk}),
            **self.admin_token
        ).status_code, status.HTTP_404_NOT_FOUND)

    def test_get_flight_settings(self):
        """ Тест получения настроек рейса
        """

        def test(keys: list):
            response = self.client.get(
                reverse('api_flight', kwargs={'pk': flight.pk}),
                **self.admin_token
            )
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self._assert_flight_data(flight, response.json(), keys)

        flight = self.get_flight_settings()
        test([])
        flight.schedule = factories.ScheduleDict()
        flight.save()
        test(['schedule'])

    def test_cannot_get_flight_settings(self):
        """ Тест ошибки получения настроек рейса
        """
        self._test_wrong_tokens(
            self.client.get, 'api_flight', {'pk': 0}
        )
        self.assertEqual(self.client.get(
            reverse('api_flight', kwargs={'pk': 0}),
            **self.client_token
        ).status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(self.client.get(
            reverse('api_flight', kwargs={'pk': 0}),
            **self.admin_token
        ).status_code, status.HTTP_404_NOT_FOUND)

    def test_list_flight_settings(self):
        """ Тест получения списка настроек рейсов
        """
        f_settings_1 = self.get_flight_settings()
        response = self._list_flights_settings(self.admin_token)
        self.assertEqual(1, len(response))
        self._assert_flight_data(f_settings_1, response[0], [])

        f_settings_2 = self.get_flight_settings()
        f_settings_2.is_approved = True
        f_settings_2.save()
        self.assertEqual(
            [f_settings_1.pk, f_settings_2.pk],
            self._list_flights_settings_id(self.admin_token)
        )
        self.assertEqual(
            [f_settings_2.pk],
            self._list_flights_settings_id(self.client_token)
        )

        # todo filters

    def test_cannot_list_flight_settings(self):
        """ Тест ошибки получения списка настроек рейсов
        """
        self._test_wrong_tokens(self.client.get, 'api_list_create_flight')
