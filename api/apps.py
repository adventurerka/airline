""" Конфиг модуля api
"""
from django.apps import AppConfig


class ApiConfig(AppConfig):
    """ Класс конфига модуля работы с api
    """
    name = 'api'
