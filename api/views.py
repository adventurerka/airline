""" Модуль views справочных методов
"""
from django.conf import settings
from rest_framework.generics import RetrieveAPIView, ListAPIView
from rest_framework.response import Response

from api.base.mixins import AdminAuthViewMixin
from core.models import Country, Plane
from . import serializers


class SettingsAPIView(RetrieveAPIView):
    """ View получения настроек сервера
    """

    def retrieve(self, request, *_, **__):
        return Response({
            'max_name_length': settings.MAX_NAME_LENGTH,
            'min_password_length': settings.MIN_PASSWORD_LENGTH
        })


class PlaneAPIView(AdminAuthViewMixin, ListAPIView):
    # pylint: disable=(no-member, too-many-ancestors)
    """ View получения списка самолетов
    """
    serializer_class = serializers.PlaneSerializer
    queryset = Plane.objects.all()


class CountryAPIView(ListAPIView):
    # pylint: disable=no-member
    """ View получения списка стран
    """
    serializer_class = serializers.CountrySerializer
    queryset = Country.objects.all()
