""" Модуль модели самолета
"""
from django.db import models
from django.utils.functional import cached_property


class Plane(models.Model):
    # pylint: disable=too-few-public-methods
    """ Класс модели самолета
    """
    class Meta:
        """ Meta
        """
        verbose_name = 'Самолет'
        verbose_name_plural = 'Самолеты'

    name = models.TextField(
        verbose_name='Название самолета',
        primary_key=True
    )
    business_rows = models.IntegerField(
        verbose_name='Количество рядов в бизнес-классе'
    )
    economy_rows = models.IntegerField(
        verbose_name='Количество рядов в эконом-классе'
    )
    places_in_business_row = models.IntegerField(
        verbose_name='Количество мест в ряду бизнес-класса'
    )
    places_in_economy_row = models.IntegerField(
        verbose_name='Количество мест в ряду эконом-класса'
    )

    @cached_property
    def economy_places(self) -> int:
        """ Количество мест эконом-класса
        """
        return self.economy_rows * self.places_in_economy_row

    @cached_property
    def business_places(self) -> int:
        """ Количество мест бизнес-класса
        """
        return self.business_rows * self.places_in_business_row
