""" Модуль модели настроек рейса
"""
import re
from datetime import timedelta, datetime

from django.conf import settings
from django.db import models


class FlightSettings(models.Model):
    # pylint: disable=too-few-public-methods
    """ Класс модели настроек рейса
    """
    class Meta:
        """ Meta
        """
        verbose_name = 'Настройки рейса'
        verbose_name_plural = 'Настройки рейсов'

    DAYS_OF_WEEK = {
        'Mon': 0, 'Tue': 1, 'Wed': 2, 'Thu': 3, 'Fri': 4, 'Sat': 5, 'Sun': 6
    }
    DAYS_OF_WEEK_REG_EX = re.compile(
        r'^((Sun|Mon|Tue|Wed|Thu|Fri|Sat),\s)*(Sun|Mon|Tue|Wed|Thu|Fri|Sat)$'
    )
    flight_name = models.TextField(
        verbose_name='Название рейса'
    )
    plane = models.ForeignKey(
        'core.Plane',
        on_delete=models.CASCADE,
        related_name='flights_settings'
    )
    from_town = models.TextField(
        verbose_name='Город вылета'
    )
    to_town = models.TextField(
        verbose_name='Город прилета'
    )
    start = models.TimeField(
        verbose_name='Время вылета'
    )
    duration = models.TimeField(
        verbose_name='Время в пути'
    )
    price_business = models.IntegerField(
        verbose_name='Цена билета в бизнес - классе'
    )
    price_economy = models.IntegerField(
        verbose_name='Цена билета в эконом - классе'
    )
    is_approved = models.BooleanField(
        verbose_name='Утверждение рейса',
        default=False
    )
    schedule = models.JSONField(
        verbose_name='Расписание',
        blank=True
    )
    dates = models.JSONField(
        verbose_name='Дни выполнения маршрута',
        blank=True
    )

    @classmethod
    def get_days_from_schedule(cls, schedule: dict) -> list:
        """ Список дат рейсов на основе расписания
        """
        period = schedule['period']
        from_date = datetime.strptime(
            schedule['from_date'], settings.DATE_FORMAT
        )
        to_date = datetime.strptime(schedule['to_date'], settings.DATE_FORMAT)

        def _get_days(func: callable) -> list:
            dates = list()
            for i in range((to_date - from_date).days + 1):
                date = from_date + timedelta(days=i)
                if func(date):
                    dates.append(date.strftime(settings.DATE_FORMAT))
            return dates

        if period == 'DAILY':
            return _get_days(lambda x: True)

        if period == 'ODD':
            return _get_days(lambda x: x.day % 2 != 0)

        if period == 'EVEN':
            return _get_days(lambda x: x.day % 2 == 0)

        days = period.split(', ')

        if cls.DAYS_OF_WEEK_REG_EX.match(period):
            items = [cls.DAYS_OF_WEEK.get(item) for item in days]
            return _get_days(lambda x: x.weekday() in items)

        items = [int(item) for item in days]
        return _get_days(lambda x: x.day in items)
