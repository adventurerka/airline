""" Модуль модели заказа
"""
from django.conf import settings
from django.db import models


class Order(models.Model):
    # pylint: disable=too-few-public-methods
    """ Класс модели заказа
    """
    class Meta:
        """ Meta
        """
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

    flight = models.ForeignKey(
        'core.Flight',
        verbose_name='Рейс',
        on_delete=models.CASCADE,
        related_name='orders'
    )
    client = models.ForeignKey(
        'core.User',
        on_delete=models.CASCADE,
        related_name='orders'
    )

    @property
    def date(self):
        # pylint: disable=no-member
        """ Дата рейса
        """
        return self.flight.date

    @property
    def flight_settings(self):
        # pylint: disable=no-member
        """ Настройки рейса
        """
        return self.flight.flight_settings

    @property
    def flight_settings_id(self):
        # pylint: disable=no-member
        """ id настроек рейса
        """
        return self.flight.flight_settings.id

    @property
    def flight_name(self):
        """ Название рейса
        """
        return self.flight_settings.flight_name

    @property
    def from_town(self):
        """ Город вылета
        """
        return self.flight_settings.from_town

    @property
    def to_town(self):
        """ Город прилета
        """
        return self.flight_settings.to_town

    @property
    def start(self):
        """ Время вылета
        """
        return self.flight_settings.start.strftime(settings.TIME_FORMAT)

    @property
    def duration(self):
        """ Время в пути
        """
        return self.flight_settings.duration.strftime(settings.TIME_FORMAT)

    @property
    def plane_name(self):
        """ Название самолета
        """
        return self.flight_settings.plane.name

    @property
    def total_price(self):
        # pylint: disable=no-member
        """ Сумма заказа
        """
        return sum(passenger.price for passenger in self.passengers.all())
