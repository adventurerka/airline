""" Модуль модели пассажира
"""
from django.db import models

from core.constants import CLASS


class Passenger(models.Model):
    # pylint: disable=too-few-public-methods
    """ Класс модели пассажира
    """
    class Meta:
        """ Meta
        """
        verbose_name = 'Пассажир'
        verbose_name_plural = 'Пассажиры'

    first_name = models.TextField(
        verbose_name='Имя'
    )
    last_name = models.TextField(
        verbose_name='Фамилия'
    )
    nationality = models.ForeignKey(
        'core.Country',
        verbose_name='Гражданство',
        on_delete=models.CASCADE,
        related_name='passengers'
    )
    passport = models.TextField(
        verbose_name='Номер паспорта'
    )
    place_class = models.TextField(
        verbose_name='Класс',
        choices=CLASS
    )
    place = models.TextField(
        verbose_name='Место',
        blank=True,
        null=True
    )
    order = models.ForeignKey(
        'core.Order',
        on_delete=models.CASCADE,
        related_name='passengers'
    )

    @property
    def price(self):
        """ Стоимость билета
        """
        # pylint: disable=no-member
        return self.order.flight_settings.price_business \
            if self.place_class == CLASS.BUSINESS \
            else self.order.flight_settings.price_economy
