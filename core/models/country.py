""" Модуль модели страны
"""
from django.db import models


class Country(models.Model):
    # pylint: disable=too-few-public-methods
    """ Класс модели страны
    """
    class Meta:
        """ Meta
        """
        verbose_name = 'Страна'
        verbose_name_plural = 'Страны'

    iso3166 = models.TextField(
        verbose_name='Код ISO 3166-1 Alpha2',
        primary_key=True
    )
    name = models.TextField(
        verbose_name='Название страны'
    )
