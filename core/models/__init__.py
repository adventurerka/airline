""" Базовые сущности для работы системы
"""
from .country import Country
from .flight import Flight
from .flight_settings import FlightSettings
from .order import Order
from .passenger import Passenger
from .plane import Plane
from .token import Token
from .user import User
