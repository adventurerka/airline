""" Модуль токена авторизации
"""
import uuid

from django.db import models
from django.conf import settings


class Token(models.Model):
    # pylint: disable=too-few-public-methods
    """ Токен авторизации
    """
    class Meta:
        """ Meta
        """
        verbose_name = 'Токен авторизации'
        verbose_name_plural = 'Токены авторизации'

    key = models.CharField(
        'Ключ',
        max_length=40,
        primary_key=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='authorization_token',
        on_delete=models.CASCADE,
        verbose_name='Пользователь',
    )

    @staticmethod
    def generate_key():
        """ Создать ключ авторизации
        """
        return str(uuid.uuid4())

    def save(self, *args, **kwargs):
        # pylint: disable=signature-differs
        """ Сохранить данные модели
        """
        if not self.key:
            self.key = self.generate_key()
        return super().save(*args, **kwargs)
