""" Модуль модели рейса
"""
import string

from django.db import models

from core.constants import CLASS


class Flight(models.Model):
    # pylint: disable=too-few-public-methods
    """ Класс модели рейса
    """

    class Meta:
        """ Meta
        """
        verbose_name = 'Рейс'
        verbose_name_plural = 'Рейсы'

    date = models.DateField(
        verbose_name='Дата'
    )
    flight_settings = models.ForeignKey(
        'core.FlightSettings',
        on_delete=models.CASCADE,
        related_name='flights'
    )
    free_place_codes = models.JSONField(
        verbose_name='Свободные коды мест',
        blank=True,
        null=True
    )

    @staticmethod
    def create_places(row_start, rows, places):
        """ Генерация списка кодов мест
        """
        return [
            f'{i + 1}{string.ascii_uppercase[j]}' for j in range(places)
            for i in range(row_start, row_start + rows)
        ]

    def set_places(self):
        # pylint: disable=no-member
        """ Создание списков кодов свободных мест
        """
        plane = self.flight_settings.plane
        business_places = self.create_places(
            0, plane.business_rows, plane.places_in_business_row
        )
        economy_places = self.create_places(
            plane.business_rows, plane.economy_rows,
            plane.places_in_economy_row
        )
        self.free_place_codes = {
            CLASS.BUSINESS: business_places,
            CLASS.ECONOMY: economy_places
        }
