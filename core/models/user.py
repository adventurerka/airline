""" Модуль модели пользователя
"""
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models
from model_utils.choices import Choices

from core.validators import UsernameValidator


class User(AbstractUser):
    # pylint: disable=too-few-public-methods
    """ Класс модели пользователя
    """
    class Meta:
        """ Meta
        """
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    REQUIRED_FIELDS = []
    USERNAME_FIELD = 'username'
    USER_TYPE = Choices(
        ('admin', 'Админ'),
        ('client', 'Клиент'),
    )
    username = models.CharField(
        verbose_name='Имя пользователя',
        validators=[UsernameValidator()],
        unique=True,
        max_length=settings.MAX_NAME_LENGTH
    )
    password = models.TextField(
        verbose_name='Пароль'
    )
    last_name = models.TextField(
        verbose_name='Фамилия'
    )
    first_name = models.TextField(
        verbose_name='Имя'
    )
    middle_name = models.TextField(
        verbose_name='Отчество',
        blank=True
    )
    user_type = models.TextField(
        verbose_name='Тип пользователя',
        choices=USER_TYPE,
        db_index=True
    )
    position = models.TextField(
        verbose_name='Должность',
        blank=True
    )
    phone = models.TextField(
        verbose_name='Номер',
        blank=True
    )
    email = models.EmailField(
        verbose_name='Почта',
        blank=True
    )

    @property
    def is_client(self) -> bool:
        """ True если пользователь клиент
        """
        return self.user_type == self.USER_TYPE.client

    @property
    def is_admin(self) -> bool:
        """ True если пользователь админ
        """
        return self.user_type == self.USER_TYPE.admin
