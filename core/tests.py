""" Тесты моделей
"""
from django.test import TestCase

from .models import FlightSettings


class FlightSettingsTestCase(TestCase):
    """ Класс тестов модели настроек рейсов
    """

    def test_get_days_from_schedule(self):
        """ Тест генерации списока дат рейсов на основе расписания
        """
        data = {
            'from_date': '2020-12-31',
            'to_date': '2021-01-04'
        }
        self.assertEqual(
            FlightSettings.get_days_from_schedule({**data, 'period': 'DAILY'}),
            [
                '2020-12-31', '2021-01-01', '2021-01-02', '2021-01-03',
                '2021-01-04'
            ]
        )
        self.assertEqual(
            FlightSettings.get_days_from_schedule({**data, 'period': 'ODD'}),
            ['2020-12-31', '2021-01-01', '2021-01-03']
        )
        self.assertEqual(
            FlightSettings.get_days_from_schedule({**data, 'period': 'EVEN'}),
            ['2021-01-02', '2021-01-04']
        )
        self.assertEqual(FlightSettings.get_days_from_schedule(
            {**data, 'period': 'Fri, Mon, Tue'}
        ), ['2021-01-01', '2021-01-04'])
        self.assertEqual(
            FlightSettings.get_days_from_schedule(
                {**data, 'period': 'Fri, Mon, Tue'}),
            ['2021-01-01', '2021-01-04']
        )
        self.assertEqual(
            FlightSettings.get_days_from_schedule(
                {**data, 'period': '31, 1, 30'}),
            ['2020-12-31', '2021-01-01']
        )
