""" Настройки модуля
"""
from django.apps import AppConfig


class CoreConfig(AppConfig):
    """ Класс конфига
    """
    name = 'core'
    verbose_name = 'Ядро'
