""" Базовые константы
"""
from model_utils.choices import Choices


CLASS = Choices(
    ('BUSINESS', 'Бизнес',),
    ('ECONOMY', 'Эконом',),
)
