""" Валидаторы
"""
# pylint: disable=too-few-public-methods
from django.conf import settings
from django.core import validators
from django.utils.deconstruct import deconstructible


@deconstructible
class UsernameValidator(validators.RegexValidator):
    """ Валидатор логина
    """
    regex = r'^[а-яА-ЯЁё\\s-]+'
    message = 'Некорректный логин$'


@deconstructible
class PhoneValidator(validators.RegexValidator):
    """ Валидатор номера
    """
    regex = r'^(\+7|8)-?(\d{3}-?){2}\d{2}-?\d{2}$'
    message = 'Некорректный номер'


@deconstructible
class PasswordValidator(validators.RegexValidator):
    """ Валидатор пароля
    """
    _len = settings.MIN_PASSWORD_LENGTH
    regex = r'^.{{{}}}$'.format(f'{8},')
    message = f'Минимальная длина пароля {_len}'


@deconstructible
class PeriodValidator(validators.RegexValidator):
    """ Валидатор периода (дней вылета)
    """
    regex = '^ODD|EVEN|DAILY|(((Sun|Mon|Tue|Wed|Thu|Fri|Sat),\\s)' \
            '*(Sun|Mon|Tue|Wed|Thu|Fri|Sat))|((\\d{1,2},\\s)*\\d{1,2})$'
    code = 'invalid_period'
    message = 'Неправильный формат дней вылета'
