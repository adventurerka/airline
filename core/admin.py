""" Модуль представлений моделей для админки
"""
from django.contrib import admin
from core.models import (
    Country, Flight, FlightSettings, Passenger, Plane, Order,
)


@admin.register(Plane)
class PlaneAdmin(admin.ModelAdmin):
    """ Представление модели самолета для админки
    """
    list_display = (
        'name', 'business_rows',  'places_in_business_row', 'economy_rows',
        'places_in_economy_row',
    )


@admin.register(Country)
class CountryAdmin(admin.ModelAdmin):
    """ Представление модели страны для админки
    """
    list_display = ('iso3166', 'name',)


@admin.register(FlightSettings)
class FlightSettingsAdmin(admin.ModelAdmin):
    """ Представление модели настроек рейса для админки
    """
    list_display = (
        'id', 'flight_name', 'is_approved', 'from_town', 'to_town',
    )


@admin.register(Flight)
class FlightAdmin(admin.ModelAdmin):
    """ Представление модели рейса для админки
    """
    list_display = ('id', 'date',)


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    """ Представление модели заказа для админки
    """
    list_display = ('id',)


@admin.register(Passenger)
class PassengerAdmin(admin.ModelAdmin):
    """ Представление модели пассажира для админки
    """
    list_display = ('id', 'first_name', 'last_name',)
