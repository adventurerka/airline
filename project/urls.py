""" Project URL Configuration
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.urls import path

from feedback.views import (
    FeedbackDetailView, FeedbackListView, FeedbackCreateView,
    FeedbackDeleteView, FeedbackUpdateView,
)

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^api/', include('api.urls')),
    path('feedback/create/', FeedbackCreateView.as_view()),
    path('feedback/delete/<int:pk>/', FeedbackDeleteView.as_view()),
    path('feedback/update/<int:pk>/', FeedbackUpdateView.as_view()),
    path('feedback/', FeedbackListView.as_view()),
    path('feedback/<int:pk>/', FeedbackDetailView.as_view()),
]
