""" Вспомогательные функции
"""


def first(sequence, key, default=None):
    """ Первый элемент из списка удовлетворяющий условию key
    """
    return next(filter(key, sequence), default)
