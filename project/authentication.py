""" Вспомогательные функции для аутентификации
"""
from django.apps import apps
from django.utils.translation import ugettext_lazy as _
from rest_framework.authentication import (
    TokenAuthentication as DRFTokenAuthentication
)
from rest_framework import exceptions


class TokenAuthentication(DRFTokenAuthentication):
    """ Авторизация по токену в строке заголовка
    """

    def get_model(self):
        """ Получить модель токена авторизации
        """
        return apps.get_model('core', 'Token')

    def authenticate_credentials(self, key):
        # pylint: disable=raise-missing-from
        """ Аутентификация пользователя
        """
        model = self.get_model()
        try:
            instance = model.objects.select_related('user').get(key=key)
        except model.DoesNotExist:
            raise exceptions.AuthenticationFailed(_('Invalid token.'))
        return instance.user, instance
