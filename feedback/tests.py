""" Модуль тестов обратной связи
"""
from django.test import TestCase


class TestAnimalListView(TestCase):
    """ Класс тестов обратной связи
    """

    def test_context(self):
        """ Тест контекста
        """
        response = self.client.get('/')
        self.assertIn('request_path', response.context)
        self.assertEqual(response.context['request_path'], '/')
