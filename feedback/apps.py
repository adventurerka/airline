""" Настройки модуля
"""
from django.apps import AppConfig


class FeedbackConfig(AppConfig):
    """ Класс конфига
    """
    name = 'feedback'
    verbose_name = 'Обратная связь'
