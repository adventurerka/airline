""" Модуль представлений модели обратной связи для админки
"""
from django.contrib import admin
from feedback.models import Feedback


@admin.register(Feedback)
class FeedbackAdmin(admin.ModelAdmin):
    """ Представление модели обратной связи для админки
    """
    list_display = ('first_name', 'last_name',  'middle_name', 'email',)
