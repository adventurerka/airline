""" Модуль модели обратной связи
"""
from django.db import models


class Feedback(models.Model):
    # pylint: disable=too-few-public-methods
    """ Класс модели обратной связи
    """

    class Meta:
        """ Meta
        """
        verbose_name = 'Обратная связь'
        verbose_name_plural = 'Обратная связь'

    title = models.CharField(
        verbose_name='Заголовок',
        max_length=100
    )
    first_name = models.CharField(
        verbose_name='Имя',
        max_length=100
    )
    last_name = models.CharField(
        verbose_name='Фамилия',
        max_length=100
    )
    middle_name = models.CharField(
        verbose_name='Отчество',
        max_length=100,
        blank=True,
        null=True
    )
    email = models.EmailField(
        verbose_name='Почта'
    )
    text = models.CharField(
        verbose_name='Текст',
        max_length=500
    )
