""" Модуль views обратной связи
"""
from django.views.generic import (
    DetailView, ListView, CreateView, DeleteView, UpdateView,
)

from feedback.models import Feedback


class FeedbackCreateView(CreateView):
    """ View создания обратной связи
    """
    model = Feedback
    template_name = 'feedback/create.html'
    success_url = '/feedback'
    fields = '__all__'


class FeedbackDetailView(DetailView):
    """ View просмотра обратной связи
    """
    model = Feedback
    template_name = 'feedback/detail.html'


class FeedbackListView(ListView):
    """ View списка обратной связи
    """
    model = Feedback
    template_name = 'feedback/list.html'


class FeedbackDeleteView(DeleteView):
    """ View удаления обратной связи
    """
    model = Feedback
    success_url = '/feedback'
    template_name = 'feedback/delete_confirm.html'


class FeedbackUpdateView(UpdateView):
    """ View редактирования обратной связи
    """
    model = Feedback
    template_name = 'feedback/create.html'
    success_url = '/feedback'
    fields = '__all__'
